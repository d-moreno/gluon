# Gluon
A tiny library that integrates engine agnostic ECS library [Baldrick](https://github.com/BlazingMammothGames/baldrick) and [Heaps](https://github.com/HeapsIO/heaps) to ease project setup and avoid a little boilerplating.

A full sample is available under `sample`, and can be compiled to Hashlink or Javascript targets with: `haxe build-sample-[target].hxml`.