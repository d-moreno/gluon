package gluon;

class GameApp extends hxd.App {
	public var currentGameScene:GameScene;
	public var screenWidth(default, null) = 800;
	public var screenHeight(default, null) = 600;
	public var fullScreen(default, null) = false;

	final override function init() {
		appInit();
		setScreenSize();

		currentGameScene = initialGameScene();
		if (currentGameScene == null) {
			throw "Initial GameScene is not set. Override setInitialGameScene and return a valid GameScene instance.";
		}
		setCurrentGameScene(currentGameScene);
	}

	/**
	 * Override and return and instance of the initial GameScene
	 */
	private function initialGameScene():GameScene {
		return null;
	}

	/**
	 * Override it and use it as a substitue for Heaps' hxd.App init()
	 */
	private function appInit() {}

	final private function setScreenSize() {
		engine.resize(screenWidth, screenHeight);

		var window:hxd.Window = hxd.Window.getInstance();
		window.resize(screenWidth, screenHeight);
		window.setFullScreen(fullScreen);
	}

	private final function setCurrentGameScene(scene:GameScene) {
		scene.init(screenWidth, screenHeight);
		setScene(scene.heaps3DScene);
		setScene(scene.heaps2DScene);
		currentGameScene = scene;
	}

	final override function update(dt:Float) {
		if (currentGameScene.isTransitioning()) {
			transitionToNextGameScene();
		}

		currentGameScene.update(dt);
	}

	final private function transitionToNextGameScene() {
		var oldGameScene = currentGameScene;
		setCurrentGameScene(currentGameScene.nextGameScene);
		oldGameScene.dispose();
	}
}
