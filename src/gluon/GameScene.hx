package gluon;

import baldrick.Phase;
import baldrick.Universe;
import hxd.Key;

class GameScene {
	public var nextGameScene:GameScene;
	public var deltaTime(default, null):Float;
	public var heaps2DScene(default, null):h2d.Scene;
	public var heaps3DScene(default, null):h3d.scene.Scene;

	private var world:Universe;

	public final function new() {}

	public final function width():Int {
		return heaps2DScene.width;
	}

	public final function height():Int {
		return heaps2DScene.height;
	}

	public final function isTransitioning():Bool {
		return nextGameScene != null;
	}

	public final function update(deltaTime:Float):Void {
		if (world == null) {
			return;
		}

		this.deltaTime = deltaTime;

		for (phase in world.phases) {
			phase.process();
		}
	}

	public final function init(screenWidth:Int, screenHeight:Int):Void {
		dispose();
		initHeapScenes(screenWidth, screenHeight);
		loadAssets();
		Key.initialize();
		initWorld();
	}

	public final function dispose() {
		if (world != null) {
			disposeWorld();
		}

		if (heaps2DScene != null) {
			heaps2DScene.dispose();
		}

		if (heaps3DScene != null) {
			heaps3DScene.dispose();
		}

		disposeAssets();
	}

	private final function disposeWorld():Void {
		world.destroyAllEntities();
		world.phases = new Array<Phase>();
	}

	/**
	 * Override to dispose assets previously loaded in "loadAssets" method, if needed
	 */
	private function disposeAssets():Void {}

	private final function initHeapScenes(screenWidth:Int, screenHeight:Int):Void {
		heaps2DScene = new h2d.Scene();
		heaps2DScene.setFixedSize(screenWidth, screenHeight);
		heaps3DScene = new h3d.scene.Scene();
	}

	/**
	 * Override to load assets that only applies to this game scene, if needed
	 */
	private function loadAssets():Void {}

	/**
	 * Override it and use it to initialize the world with entities and processors
	 */
	private function initWorld():Void {}
}
