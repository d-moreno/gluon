package processors;

import baldrick.Processor;
import baldrick.View;
import components.PlayerProperties;
import components.Position;
import components.Size;
import components.Speed;
import components.UserInputControllability;
import components.Velocity;
import gluon.GameScene;
import hxd.Key;

class PlayerMovementProcessor implements Processor {
	var playerMovableEntities:View<{
		playerProperties:PlayerProperties,
		userInputControllability:UserInputControllability,
		speed:Speed,
		velocity:Velocity,
		position:Position,
		size:Size
	}> = new View<{
		playerProperties:PlayerProperties,
		userInputControllability:UserInputControllability,
		speed:Speed,
		velocity:Velocity,
		position:Position,
		size:Size
	}>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (playerMovableEntity in playerMovableEntities) {
			var playerProperties:PlayerProperties = playerMovableEntity.data.playerProperties;
			var speed:Speed = playerMovableEntity.data.speed;
			var velocity:Velocity = playerMovableEntity.data.velocity;
			var position:Position = playerMovableEntity.data.position;
			var size:Size = playerMovableEntity.data.size;

			var topBoardPosition:Float = position.y;
			var bottomBoardPosition:Float = position.y + size.height;

			velocity.y = 0;
			if (playerProperties.player == PlayerProperties.PLAYER_1 && Key.isDown(Key.Q) && topBoardPosition >= 0) {
				velocity.y = speed.speed * currentScene.deltaTime * -1;
			} else if (playerProperties.player == PlayerProperties.PLAYER_1 && Key.isDown(Key.A) && bottomBoardPosition <= currentScene.height()) {
				velocity.y = speed.speed * currentScene.deltaTime * 1;
			} else if (playerProperties.player == PlayerProperties.PLAYER_2 && Key.isDown(Key.UP) && topBoardPosition >= 0) {
				velocity.y = speed.speed * currentScene.deltaTime * -1;
			} else if (playerProperties.player == PlayerProperties.PLAYER_2 && Key.isDown(Key.DOWN) && bottomBoardPosition <= currentScene.height()) {
				velocity.y = speed.speed * currentScene.deltaTime * 1;
			}
		}
	}
}
