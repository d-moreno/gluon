package processors;

import baldrick.ProcessorTypeID;
import baldrick.Processor;
import baldrick.View;
import components.BallProperties;
import components.Position;
import components.Size;
import components.Velocity;
import gluon.GameScene;
import hxd.res.Sound;

class BallWallBounceProcessor implements Processor {
	var ballEntities:View<{
		ballProperties:BallProperties,
		position:Position,
		velocity:Velocity,
		size:Size
	}> = new View<{
		ballProperties:BallProperties,
		position:Position,
		velocity:Velocity,
		size:Size
	}>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (ballEntity in ballEntities) {
			var position:Position = ballEntity.data.position;
			var velocity:Velocity = ballEntity.data.velocity;
			var size:Size = ballEntity.data.size;

			if (position.y + velocity.y * currentScene.deltaTime - size.height / 2 < 0) {
				velocity.y = Math.abs(velocity.y);
				playSound();
			} else if (position.y + velocity.y * currentScene.deltaTime + size.height / 2 > currentScene.height()) {
				velocity.y = -Math.abs(velocity.y);
				playSound();
			}
		};
	}

	private function playSound():Void {
		var sound:Sound = null;
		if (hxd.res.Sound.supportedFormat(Mp3)) {
			sound = hxd.Res.load("audio/wall_bounce.mp3").toSound();
		} else if (hxd.res.Sound.supportedFormat(OggVorbis)) {
			sound = hxd.Res.load("audio/wall_bounce.ogg").toSound();
		}

		if (sound != null) {
			sound.play();
		}
	}
}
