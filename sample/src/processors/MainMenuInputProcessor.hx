package processors;

import baldrick.Processor;
import gameScenes.Pong;
import gluon.GameScene;
import hxd.Key;

class MainMenuInputProcessor implements Processor {
	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		if (Key.isDown(Key.ENTER)) {
			currentScene.nextGameScene = new Pong();
		} else if (Key.isDown(Key.ESCAPE)) {
			#if !js
			hxd.System.exit();
			#end
		}
	}
}
