package processors;

import baldrick.View;
import baldrick.Processor;
import components.BallProperties;
import components.PlayerProperties;
import components.Position;
import components.Velocity;
import gluon.GameScene;

class MovementProcessor implements Processor {
	var ballEntities:View<{
		position:Position,
		velocity:Velocity,
		ballProperties:BallProperties
	}> = new View<{
		position:Position,
		velocity:Velocity,
		ballProperties:BallProperties
	}>();
	var playerEntities:View<{
		position:Position,
		velocity:Velocity,
		playerProperties:PlayerProperties
	}> = new View<{
		position:Position,
		velocity:Velocity,
		playerProperties:PlayerProperties
	}>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (ballEntity in ballEntities) {
			var ballProperties:BallProperties = ballEntity.data.ballProperties;
			if (ballProperties.spawnTime <= Date.now().getTime()) {
				moveEntity(ballEntity.data.position, ballEntity.data.velocity);
			}
		};

		for (playerEntity in playerEntities) {
			moveEntity(playerEntity.data.position, playerEntity.data.velocity);
		};
	}

	private function moveEntity(position:Position, velocity:Velocity):Void {
		position.x += velocity.x * currentScene.deltaTime;
		position.y += velocity.y * currentScene.deltaTime;
	}
}
