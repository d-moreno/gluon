package processors;

import baldrick.Processor;
import baldrick.View;
import components.BallProperties;
import components.CollideAbility;
import components.PlayerProperties;
import components.Position;
import components.Size;
import components.Velocity;
import Date;
import gluon.GameScene;
import h2d.col.Bounds;
import h2d.col.Point;
import hxd.res.Sound;

private typedef PlayerEntityData = {
	playerCollideAbility:CollideAbility,
	playerProperties:PlayerProperties,
	playerPosition:Position,
	playerSize:Size
};

private typedef BallEntityData = {
	ballCollideAbility:CollideAbility,
	ballVelocity:Velocity,
	ballProperties:BallProperties
};

class PlayerAndBallCollisionProcessor implements Processor {
	var playerEntities:View<{
		playerCollideAbility:CollideAbility,
		playerProperties:PlayerProperties,
		playerPosition:Position,
		playerSize:Size
	}> = new View<PlayerEntityData>();
	var ballEntities:View<{
		ballCollideAbility:CollideAbility,
		ballVelocity:Velocity,
		ballProperties:BallProperties
	}> = new View<BallEntityData>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (ballEntity in ballEntities) {
			var ballCollideAbility:CollideAbility = ballEntity.data.ballCollideAbility;
			var ballBounds:Bounds = ballCollideAbility.drawableObject.getBounds();

			for (playerEntity in playerEntities) {
				var playerCollideAbility:CollideAbility = playerEntity.data.playerCollideAbility;
				var playerBoardBounds:Bounds = playerCollideAbility.drawableObject.getBounds();

				if (!ballCollideAbility.hadRecentCollission() && ballBounds.intersects(playerBoardBounds)) {
					triggerCollision(ballEntity.data, playerEntity.data);
				}
			}
		}
	}

	private function triggerCollision(ballEntity:BallEntityData, playerEntity:PlayerEntityData):Void {
		ballEntity.ballCollideAbility.lastCollisionTime = Date.now().getTime();
		playerEntity.playerCollideAbility.lastCollisionTime = ballEntity.ballCollideAbility.lastCollisionTime;
		setBallVelocity(ballEntity, playerEntity);
		playSound();
	}

	private function setBallVelocity(ballEntity:BallEntityData, playerEntity:PlayerEntityData):Void {
		var ballBounds:Bounds = ballEntity.ballCollideAbility.drawableObject.getBounds();
		var playerBoardBounds:Bounds = playerEntity.playerCollideAbility.drawableObject.getBounds();

		var collissionPoint:Point = ballBounds.intersection(playerBoardBounds).getCenter();
		var relativeCollissionPoint = collissionPoint.y - playerEntity.playerPosition.y;
		var relativeCollissionPointAsPercent = (relativeCollissionPoint - playerEntity.playerSize.height / 2) * 100 / (playerEntity.playerSize.height / 2);
		ballEntity.ballVelocity.x = -ballEntity.ballVelocity.x;
		ballEntity.ballVelocity.y = Velocity.BALL_DEFAULT_VELOCITY * relativeCollissionPointAsPercent / 100;
	}

	private function playSound():Void {
		var sound:Sound = null;
		if (hxd.res.Sound.supportedFormat(Mp3)) {
			sound = hxd.Res.load("audio/board_bounce.mp3").toSound();
		} else if (hxd.res.Sound.supportedFormat(OggVorbis)) {
			sound = hxd.Res.load("audio/board_bounce.ogg").toSound();
		}

		if (sound != null) {
			sound.play();
		}
	}
}
