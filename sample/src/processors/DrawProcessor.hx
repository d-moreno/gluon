package processors;

import baldrick.Processor;
import baldrick.View;
import components.Drawability;
import components.Position;
import gluon.GameScene;

class DrawProcessor implements Processor {
	var drawableEntities:View<{drawability:Drawability, position:Position}> = new View<{drawability:Drawability, position:Position}>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (drawableEntity in drawableEntities) {
			drawDrawable(drawableEntity.data.drawability, drawableEntity.data.position);
			drawText(drawableEntity.data.drawability, drawableEntity.data.position);
		}
	}

	private function drawDrawable(drawability:Drawability, position:Position):Void {
		if (drawability.drawableObject == null) {
			return;
		}

		drawability.drawableObject.x = position.x;
		drawability.drawableObject.y = position.y;

		drawability.drawableObject.visible = true;
		if (drawability.spawnTime > Date.now().getTime()) {
			drawability.drawableObject.visible = false;
		}
	}

	private function drawText(drawability:Drawability, position:Position):Void {
		if (drawability.text == null) {
			return;
		}

		drawability.text.x = position.x;
		drawability.text.y = position.y;

		drawability.text.visible = true;
		if (drawability.spawnTime > Date.now().getTime()) {
			drawability.text.visible = false;
		}
	}
}
