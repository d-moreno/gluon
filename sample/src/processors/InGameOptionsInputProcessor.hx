package processors;

import baldrick.Processor;
import gameScenes.MainMenu;
import gluon.GameScene;
import hxd.Key;

class InGameOptionsInputProcessor implements Processor {
	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		if (Key.isDown(Key.ESCAPE)) {
			currentScene.nextGameScene = new MainMenu();
		}
	}
}
