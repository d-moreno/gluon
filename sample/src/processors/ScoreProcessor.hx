package processors;

import baldrick.Entity;
import baldrick.Processor;
import baldrick.View;
import components.BallProperties;
import components.Drawability;
import components.PlayerProperties;
import components.Position;
import components.ScoreBoardProperties;
import components.Size;
import gluon.GameScene;
import hxd.res.Sound;

private typedef BallEntityData = {
	ballProperties:BallProperties,
	ballDrawability:Drawability,
	ballPosition:Position,
	ballSize:Size
};

class ScoreProcessor implements Processor {
	var scoreBoardEntities:View<{
		scoreBoardProperties:ScoreBoardProperties
	}> = new View<{
		scoreBoardProperties:ScoreBoardProperties
	}>();
	var ballEntities:View<{
		ballProperties:BallProperties,
		ballDrawability:Drawability,
		ballPosition:Position,
		ballSize:Size
	}> = new View<BallEntityData>();

	public var currentScene(default, null):GameScene;

	public function new(currentScene:GameScene) {
		this.currentScene = currentScene;
	}

	public function process():Void {
		for (ballEntity in ballEntities) {
			var ballPosition:Position = ballEntity.data.ballPosition;
			var ballSize:Size = ballEntity.data.ballSize;

			if (ballPosition.x <= 0) {
				scorePoint(ballEntity.data, PlayerProperties.PLAYER_2);
			} else if ((ballPosition.x + ballSize.width) >= currentScene.width()) {
				scorePoint(ballEntity.data, PlayerProperties.PLAYER_1);
			}
		}
	}

	private function scorePoint(ballEntity:BallEntityData, playerNumber:Int):Void {
		for (scoreBoardEntity in scoreBoardEntities) {
			var scoreBoardProperties:ScoreBoardProperties = scoreBoardEntity.data.scoreBoardProperties;
			if (scoreBoardProperties.player == playerNumber) {
				scoreBoardProperties.score++;
				scoreBoardProperties.scoreText.text = "" + scoreBoardProperties.score;
				break;
			}
		}
		resetBall(ballEntity);
		playSound();
	}

	private function resetBall(ballEntity:BallEntityData):Void {
		ballEntity.ballPosition.x = currentScene.width() / 2 - ballEntity.ballSize.width / 2;
		ballEntity.ballPosition.y = Std.random(currentScene.height());

		var spawnTime = Date.now().getTime() + BallProperties.DEFAULT_SPAWN_TIME;

		ballEntity.ballProperties.spawnTime = spawnTime;
		ballEntity.ballDrawability.spawnTime = spawnTime;
	}

	private function playSound():Void {
		var sound:Sound = null;
		if (hxd.res.Sound.supportedFormat(Mp3)) {
			sound = hxd.Res.load("audio/score.mp3").toSound();
		} else if (hxd.res.Sound.supportedFormat(OggVorbis)) {
			sound = hxd.Res.load("audio/score.ogg").toSound();
		}

		if (sound != null) {
			sound.play();
		}
	}
}
