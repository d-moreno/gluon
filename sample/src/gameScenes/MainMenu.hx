package gameScenes;

import baldrick.Phase;
import baldrick.Universe;
import components.Drawability;
import components.Position;
import gluon.GameScene;
import processors.DrawProcessor;
import processors.MainMenuInputProcessor;

class MainMenu extends GameScene {
	private override function initWorld() {
		world = new Universe();
		addProcessors();

		addTitle();
		addControlsText();
	}

	private function addProcessors():Void {
		var processors:Phase = world.createPhase();
		processors.addProcessor(new DrawProcessor(this));
		processors.addProcessor(new MainMenuInputProcessor(this));
	}

	private function addTitle():Void {
		var text = new h2d.Text(hxd.Res.fonts.PressStart2P_32.toFont());
		text.text = "Pong Sample";
		text.textColor = 0xFFFFFF;
		text.textAlign = Center;
		text.scale(2);
		text.visible = false;
		heaps2DScene.addChild(text);

		var drawability:Drawability = new Drawability();
		drawability.setText(text);

		var position = new Position(width() / 2, text.textHeight * 3);

		world.createEntity([position, drawability]);
	}

	private function addControlsText():Void {
		var text = new h2d.Text(hxd.Res.fonts.PressStart2P_32.toFont());

		text.text = "Player 1 controls: <Q><A>\nPlayer 2 controls: <▲>️<▼>\n\nPress <Enter> to start";
		text.textColor = 0xFFFFFF;
		text.textAlign = Center;
		text.scale(0.7);
		text.visible = false;
		heaps2DScene.addChild(text);

		var drawability:Drawability = new Drawability();
		drawability.setText(text);

		var position = new Position(width() / 2, height() - text.textHeight * 1.5);

		world.createEntity([position, drawability]);
	}
}
