package gameScenes;

import baldrick.Phase;
import baldrick.Universe;
import components.BallProperties;
import components.CollideAbility;
import components.Drawability;
import components.PlayerProperties;
import components.Position;
import components.ScoreBoardProperties;
import components.Size;
import components.Speed;
import components.UserInputControllability;
import components.Velocity;
import gluon.GameScene;
import h2d.Text;
import processors.BallWallBounceProcessor;
import processors.DrawProcessor;
import processors.MovementProcessor;
import processors.PlayerAndBallCollisionProcessor;
import processors.PlayerMovementProcessor;
import processors.ScoreProcessor;
import processors.InGameOptionsInputProcessor;

class Pong extends GameScene {
	private var boardWidth:Int;
	private var boardHeight:Int;

	private override function initWorld() {
		world = new Universe();
		addProcessors();

		boardWidth = Std.int(width() / 80);
		boardHeight = Std.int(height() / 12);
		addBall();
		var player1X:Int = boardWidth;
		var player1Y:Int = Std.int(height() / 2 - boardHeight / 2);
		var player2X:Int = Std.int(width() - boardWidth - boardWidth);
		var player2Y:Int = Std.int(height() / 2 - boardHeight / 2);
		addPlayer(PlayerProperties.PLAYER_1, player1X, player1Y);
		addPlayer(PlayerProperties.PLAYER_2, player2X, player2Y);
		addPlayerScoreBoard(PlayerProperties.PLAYER_1, Std.int(width() / 4), 10);
		addPlayerScoreBoard(PlayerProperties.PLAYER_2, Std.int((width() / 4) * 3), 10);
		addDecorations();
	}

	private function addProcessors():Void {
		var processors:Phase = world.createPhase();
		processors.addProcessor(new InGameOptionsInputProcessor(this));
		processors.addProcessor(new MovementProcessor(this));
		processors.addProcessor(new BallWallBounceProcessor(this));
		processors.addProcessor(new PlayerAndBallCollisionProcessor(this));
		processors.addProcessor(new DrawProcessor(this));
		processors.addProcessor(new PlayerMovementProcessor(this));
		processors.addProcessor(new ScoreProcessor(this));
	}

	private function addBall():Void {
		var appearance = new h2d.Graphics(heaps2DScene);
		appearance.beginFill(0xFFFFFF);
		appearance.drawRect(0, 0, boardWidth, boardWidth);
		appearance.endFill();
		appearance.visible = false;
		var drawability = new Drawability();
		drawability.setDrawable(appearance);

		var ballProperties = new BallProperties();
		var size = new Size(boardWidth, boardWidth);
		var position = new Position(width() / 2 - boardWidth / 2, height() / 2 - boardWidth / 2);
		var velocity = randomizeBallInitialVelocity();
		var collideAbility = new CollideAbility(appearance);

		world.createEntity([ballProperties, position, velocity, collideAbility, size, drawability]);
	}

	private function randomizeBallInitialVelocity() {
		var xSense = Std.random(2) == 0 ? -1 : 1;
		var ySense = Std.random(2) == 0 ? -1 : 1;
		return new Velocity(Velocity.BALL_DEFAULT_VELOCITY * xSense, Velocity.BALL_DEFAULT_VELOCITY * ySense);
	}

	private function addPlayer(player:Int, x:Int, y:Int):Void {
		var appearance = new h2d.Graphics(heaps2DScene);
		appearance.beginFill(0xFFFFFF);
		appearance.drawRect(0, 0, boardWidth, boardHeight);
		appearance.endFill();
		appearance.visible = false;

		var playerProperties = new PlayerProperties(player);
		var size = new Size(boardWidth, boardHeight);
		var position = new Position(x, y);
		var velocity = new Velocity(0, 0);
		var speed = new Speed(Speed.PLAYER_DEFAULT_SPEED);
		var input = new UserInputControllability();
		var collide = new CollideAbility(appearance);

		var drawability = new Drawability();
		drawability.setDrawable(appearance);

		world.createEntity([playerProperties, position, velocity, speed, input, collide, size, drawability]);
	}

	private function addDecorations():Void {
		var position = new Position(width() / 2, 0);

		var appearance = new h2d.Graphics(heaps2DScene);
		appearance.beginFill(0xFFFFFF);
		appearance.alpha = 0.5;
		appearance.drawRect(0, 0, 1, height());
		appearance.endFill();
		appearance.visible = false;

		var drawability = new Drawability();
		drawability.setDrawable(appearance);

		world.createEntity([position, drawability]);
	}

	private function addPlayerScoreBoard(player:Int, x:Int, y:Int):Void {
		var position = new Position(x, y);

		var text = new h2d.Text(hxd.Res.fonts.PressStart2P_32.toFont());
		text.text = "0";
		text.textColor = 0xFFFFFF;
		text.textAlign = Center;
		text.scale(2);
		text.visible = false;
		heaps2DScene.addChild(text);

		var drawability:Drawability = new Drawability();
		drawability.setText(text);

		var scoreBoardProperties = new ScoreBoardProperties(player, text);

		world.createEntity([position, drawability, scoreBoardProperties]);
	}
}
