package;

import gameScenes.MainMenu;
import gluon.GameScene;
import gluon.GameApp;

class Main extends GameApp {
	static function main() {
		new Main();
	}

	private override function initialGameScene():GameScene {
		return new MainMenu();
	}

	private override function appInit() {
		screenWidth = 1024;
		screenHeight = 576;
		fullScreen = true;

		hxd.Res.initEmbed();
	}
}
