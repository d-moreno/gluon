package components;

import baldrick.Component;

class Velocity implements Component {
	public static var BALL_DEFAULT_VELOCITY:Int = 300;

	public var x:Float;
	public var y:Float;

	public function new(x:Float, y:Float) {
		this.x = x;
		this.y = y;
	}
}
