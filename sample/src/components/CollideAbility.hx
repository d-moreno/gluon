package components;

import baldrick.Component;
import h2d.Drawable;

class CollideAbility implements Component {
	public var lastCollisionTime:Float = Date.now().getTime();
	public var drawableObject:Drawable;

	public function new(drawableObject:Drawable) {
		this.drawableObject = drawableObject;
	}

	public function hadRecentCollission():Bool {
		return Date.now().getTime() - lastCollisionTime <= 1000;
	}
}
