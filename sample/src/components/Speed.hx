package components;

import baldrick.Component;

class Speed implements Component {
	public static var PLAYER_DEFAULT_SPEED:Int = 40000;

	public var speed:Float;

	public function new(speed:Float)
		this.speed = speed;
}
