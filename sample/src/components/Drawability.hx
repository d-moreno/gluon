package components;

import baldrick.Component;
import h2d.Drawable;
import h2d.Text;

class Drawability implements Component {
	public var drawableObject(default, null):Drawable;
	public var text(default, null):Text;
	public var spawnTime:Float = 0;

	public function new() {}

	public function setDrawable(drawable:Drawable) {
		this.drawableObject = drawable;
	}

	public function setText(text:Text) {
		this.text = text;
	}
}
