package components;

import baldrick.Component;

class Size implements Component {
	public var width:Float;
	public var height:Float;

	public function new(width:Float, height:Float) {
		this.width = width;
		this.height = height;
	}
}
