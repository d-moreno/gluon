package components;

import baldrick.Component;

class PlayerProperties implements Component {
	public static var PLAYER_1:Int = 10;
	public static var PLAYER_2:Int = 20;

	public var player:Int;
	public var score:Int;

	public function new(player:Int) {
		this.player = player;
		this.score = 0;
	}
}
