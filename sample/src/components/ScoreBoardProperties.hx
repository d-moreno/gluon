package components;

import baldrick.Component;
import h2d.Text;

class ScoreBoardProperties implements Component {
	public var player:Int;
	public var score:Int;
	public var scoreText:Text;

	public function new(player:Int, scoreText:Text) {
		this.player = player;
		this.scoreText = scoreText;
		this.score = 0;
	}
}
